export APP_NAME_DOCKER='monitoring'
export DOCKER_NETWORK="nginx-proxy"

export APP_VOLUME_FILES_MOUNT_PATH="/home/tuantien2k1/data/web-data/${APP_NAME_DOCKER}/filebrowser"
export APP_VOLUME_DASHBOARD_MOUNT_PATH="/home/tuantien2k1/data/web-data/${APP_NAME_DOCKER}/dashboard"

export FILES_HOST='files.hstore.pw'
export DASHBOARD_HOST='dashboard.hstore.pw'
